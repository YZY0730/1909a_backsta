export default [
    {
        exact: true,
        path: '/',
        name: '工作台',
        icon: "DashboardOutlined",
        component: '@/pages/index.tsx'
    },
    {
        path: "/Article",
        name: "文章管理",
        icon: "form",
        routes: [
            {
                path: '/Article/allArticle',
                name: '所有文章',
                icon: 'Form',
                component: '@/pages/Article/allArticle.tsx'
            },
            {
                path: '/Article/Classify',
                name: '分类管理',
                icon: 'copy',
                component: '@/pages/Article/Classify.tsx'
            },
            {
                path: '/Article/Tags',
                name: '标签管理',
                icon: 'tag',
                component: '@/pages/Article/Tags.tsx'
            }
        ]
    },
    {
        exact: true,
        path: "/Page",
        name: "知识小册",
        icon: 'book',
        component: "@/pages/Page/index.tsx"
    }
    ,
    {
        exact: true,
        path: "/Knowledge",
        name: "页面管理",
        icon: 'snippets',
        component: "@/pages/Knowledge/index.tsx"
    },
    {
        exact: true,
        path: "/Poster",
        name: "海报管理",
        icon: 'star',
        component: "@/pages/Poster/index.tsx"
    },
    {
        exact: true,
        path: "/Comment",
        name: "评论管理",
        icon: 'message',
        component: "@/pages/Comment/index.tsx"
    },
    {
        exact: true,
        path: "/Mailbox",
        name: "邮箱管理",
        icon: 'mail',
        component: "@/pages/Mailbox/index.tsx"
    },
    {
        exact: true,
        path: "/File",
        name: "文件管理",
        icon: 'folderOpen',
        component: "@/pages/File/index.tsx"
    },
    {
        exact: true,
        path: "/SearchRecord",
        name: "搜索记录",
        icon: 'search',
        component: "@/pages/SearchRecord/index.tsx"
    },
    {
        exact: true,
        path: "/AccessStatistics",
        name: "访问统计",
        icon: 'project',
        component: "@/pages/AccessStatistics/index.tsx"
    },
    {
        exact: true,
        path: "/User",
        name: "用户管理",
        icon: 'user',
        component: "@/pages/User/index.tsx"
    },
    {
        exact: true,
        path: "/systemSetup",
        name: "系统设置",
        icon: 'setting',
        component: "@/pages/systemSetup/index.tsx"
    },
    {
        exact: true,
        path: "/Login",
        name: "登录",
        component: "@/pages/Login",
        // 新页面打开
        target: '_blank',
        // 不展示顶栏
        headerRender: false,
        // 不展示页脚
        footerRender: false,
        // 不展示菜单
        menuRender: false,
        // 不展示菜单顶栏
        menuHeaderRender: false,
        // 权限配置，需要与 plugin-access 插件配合使用
        access: 'canRead',
        // 隐藏子菜单
        hideChildrenInMenu: true,
        // 隐藏自己和子菜单
        hideInMenu: true,
        // 在面包屑中隐藏
        hideInBreadcrumb: true,
        // 子项往上提，仍旧展示,
        flatMenu: true,
    }
];