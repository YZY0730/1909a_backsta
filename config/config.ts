
import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
  layout: {
    name: '后台管理',
    // locale: true,
    // layout: 'side',
    logo:"https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png"
  },
});